package ru.will0376.ctmodules.ctwebserver;

import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import ru.will0376.ctmodules.ctwebserver.webserver.WBSMakeScreen;
import ru.will0376.ctmodules.events.EventPrintHelp;
import ru.will0376.ctmodules.events.EventReceiveScreen;
import ru.will0376.ctmodules.events.EventReloadConfig;
import ru.will0376.ctmodules.events.utils.Logger;
import ru.will0376.ctmodules.events.utils.Utils;
import sun.misc.BASE64Decoder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;

@Mod.EventBusSubscriber
public class Events {
	@SubscribeEvent
	public static void catchLink(EventReceiveScreen e) {
		try {
			if (e.isWbs()) {
				if (Ctwebserver.WBSabstr.containsKey(e.getPlayerNick())) {
					String imgb64 = e.getJo().get("imgb64").getAsString();

					BASE64Decoder decoder = new BASE64Decoder();
					byte[] imageByte = decoder.decodeBuffer(imgb64);
					ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
					BufferedImage image = ImageIO.read(bis);
					bis.close();

					((WBSMakeScreen) Ctwebserver.WBSabstr.get(e.getPlayerNick())).image = image;
				}

			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@SubscribeEvent
	public static void printHelp(EventPrintHelp e) {
		e.printToPlayer(Utils.makeUsage(Ctwebserver.MOD_ID, Commands.usage, Commands.aliases, Commands.permissions));
	}

	@SubscribeEvent
	public static void reload(EventReloadConfig e) {
		Ctwebserver.config.launch();
		if (Ctwebserver.webServer != null) Ctwebserver.webServer.stop(0);
		if (Ctwebserver.webServerTh != null) Ctwebserver.webServerTh.stop();
		Ctwebserver.webServerTh = null;
		Ctwebserver.INSTANCE.runWebServer();

		if (Utils.getEPMP(e.getSender()) == null) Logger.log(1, "[Ctwebserver]", "Reloaded");
		else
			Utils.getEPMP(e.getSender()).sendMessage(new TextComponentString(TextFormatting.GOLD + "[Ctwebserver] Reloaded"));
	}
}
