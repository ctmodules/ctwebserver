package ru.will0376.ctmodules.ctwebserver;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class WBSAuthLog {
	public static File pathToWBSAL;

	public WBSAuthLog(String path) {
		pathToWBSAL = new File(path + "/WBSAuthLog/");
		if (!pathToWBSAL.exists()) pathToWBSAL.mkdir();
	}

	public static void authFail(String text) {
		printToFile(text, "Fail.log");
	}

	public static void authSuccess(String text) {
		printToFile(text, "Success.log");
	}

	private static void printToFile(String text, String filename) {
		try {
			String timeStamp = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(Calendar.getInstance().getTime());
			File tmp = new File(pathToWBSAL.getAbsolutePath() + File.separator + filename);
			checkFile(tmp);
			PrintWriter out = new PrintWriter(new FileWriter(tmp, true));
			out.println(String.format("[%s] ", timeStamp) + text + "\n");
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void checkFile(File tmp) throws IOException {
		if (!tmp.exists()) {
			tmp.createNewFile();
		}
	}
}

