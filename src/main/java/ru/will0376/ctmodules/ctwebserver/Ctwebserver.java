package ru.will0376.ctmodules.ctwebserver;

import com.sun.net.httpserver.HttpServer;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import ru.will0376.ctmodules.ctwebserver.webserver.*;
import ru.will0376.ctmodules.events.EventLogPrint;
import ru.will0376.ctmodules.events.EventModStatusResponse;

import java.net.InetSocketAddress;
import java.util.HashMap;

@Mod(
		modid = Ctwebserver.MOD_ID,
		name = Ctwebserver.MOD_NAME,
		version = Ctwebserver.VERSION,
		dependencies = "required-after:events@[1.0,)",
		acceptableRemoteVersions = "*",
		serverSideOnly = true
)
public class Ctwebserver {

	public static final String MOD_ID = "ctwebserver";
	public static final String MOD_NAME = "Ctwebserver";
	public static final String VERSION = "1.0";
	public static Config config;
	public static HashMap<String, WBSAbstraction> WBSabstr = new HashMap<>(); //user,instance.
	public static Thread webServerTh;
	public static HttpServer webServer;
	@Mod.Instance(MOD_ID)
	public static Ctwebserver INSTANCE;

	@Mod.EventHandler
	public void preinit(FMLPreInitializationEvent event) {
		config = new Config(event.getSuggestedConfigurationFile());
		config.launch();
	}

	@Mod.EventHandler
	public void onServerStarting(FMLServerStartingEvent event) {
		event.registerServerCommand(new Commands());
		runWebServer();
		if (config.isEnabled()) new WBSAuthLog(event.getServer().getDataDirectory().getAbsolutePath());
	}

	@Mod.EventHandler
	public void postinit(FMLPostInitializationEvent event) {
		MinecraftForge.EVENT_BUS.post(new EventLogPrint(3, TextFormatting.GOLD + "[Ctwebserver]" + TextFormatting.RESET, "PostInit done!"));
		MinecraftForge.EVENT_BUS.post(new EventModStatusResponse(MOD_ID, config.isEnabled()));
	}

	public void runWebServer() {
		if (config.isEnabled()) {
			webServerTh = new Thread(() -> {
				try {
					webServer = HttpServer.create();
					webServer.bind(new InetSocketAddress(config.getPort()), 0);
					webServer.createContext("/", new WBSHandler()).setAuthenticator(new WBSAuth("/"));
					webServer.createContext("/makeScreen", new WBSMakeScreen()).setAuthenticator(new WBSAuth("/makeScreen"));
					webServer.createContext("/makeCrash", new WBSMakeCrash()).setAuthenticator(new WBSAuth("/makeCrash"));
					webServer.start();
				} catch (Exception e) {
					e.printStackTrace();
				}
			});
			webServerTh.start();
		}
		MinecraftForge.EVENT_BUS.post(new EventLogPrint(3, "[Main-WebServer-Starter]", "WebServer started!"));
	}

}
