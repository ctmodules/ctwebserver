package ru.will0376.ctmodules.ctwebserver.webserver;

import com.sun.net.httpserver.HttpExchange;
import net.minecraftforge.common.MinecraftForge;
import ru.will0376.ctmodules.ctwebserver.Ctwebserver;
import ru.will0376.ctmodules.events.EventLogPrint;
import ru.will0376.ctmodules.events.EventRequestScreen;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Base64;
import java.util.Map;

public class WBSMakeScreen extends WBSAbstraction {
	//public String link = "null";
	public BufferedImage image = null;
	public long starttime;
	public int timeout = Ctwebserver.config.getTimeout();

	public WBSMakeScreen() {
		wbsae = WBSAE.SCREEN;
	}

	public static String imgToBase64String(RenderedImage img) {
		final ByteArrayOutputStream os = new ByteArrayOutputStream();

		try {
			ImageIO.write(img, "png", os);
			return Base64.getEncoder().encodeToString(os.toByteArray());
		} catch (final IOException ioe) {
			throw new UncheckedIOException(ioe);
		}
	}

	@Override
	public void handle(HttpExchange httpExchange) throws IOException {
		String encoding = "UTF-8";
		httpExchange.getResponseHeaders().set("Content-Type", "text/html; charset=" + encoding);
		StringBuilder builder = new StringBuilder();
		Map<String, String> params = queryToMap(httpExchange.getRequestURI().getQuery());
		if (params.isEmpty()) return;
		MinecraftForge.EVENT_BUS.post(new EventRequestScreen(params.get("admin"), params.get("user"), true));
		Ctwebserver.WBSabstr.put(params.get("user"), this);
		starttime = System.currentTimeMillis();
		while (true) {
			if (image != null) {
				MinecraftForge.EVENT_BUS.post(new EventLogPrint(3, "[WBSMakeScreen]", "break by image != null"));
				break;
			}
			if (System.currentTimeMillis() - starttime > (timeout * 1000)) {
				MinecraftForge.EVENT_BUS.post(new EventLogPrint(3, "[WBSMakeScreen]", "break by timeout."));
				break;
			}
		}

		if (image != null) {
			builder.append("<table border=\"1\">");
			builder.append("<tr>");
			builder.append("<td>").append(params.get("user")).append("</td>");
			builder.append("<td>").append(getEPMP(params.get("user")).getPosition().toString()).append("</td>");
			builder.append("<td>").append(getEPMP(params.get("user")).getEntityWorld().getWorldInfo().getWorldName()).append("</td>");
			builder.append("<td>").append("<button onClick=\"window.location.reload();\">Refresh Page</button>").append("</td>");
			builder.append("<td>").append("<a href=\"/").append("\">").append("<button>Back</button>").append("</a>").append("</td>");

			builder.append("</tr>");
			builder.append("</table>");

			//builder.append("<iframe src=\"").append(link).append("\" height=100%, width=100%></iframe>");
			builder.append("<iframe src=\"data:image/png;base64,").append(imgToBase64String(image)).append("\" height=100%, width=100%></iframe>");

		} else {
			builder.append("<h1>Time Out!</h1>").append("<button onClick=\"window.location.reload();\">Refresh Page</button>");
		}
		Ctwebserver.WBSabstr.remove(params.get("user"));
		image = null;
		sendToHttpEx(httpExchange, builder);

	}
}

