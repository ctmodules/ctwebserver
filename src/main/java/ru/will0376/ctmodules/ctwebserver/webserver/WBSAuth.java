package ru.will0376.ctmodules.ctwebserver.webserver;

import com.sun.net.httpserver.BasicAuthenticator;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpPrincipal;
import ru.will0376.ctmodules.ctwebserver.Ctwebserver;
import ru.will0376.ctmodules.ctwebserver.WBSAuthLog;

import java.util.Base64;

public class WBSAuth extends BasicAuthenticator {
	public WBSAuth(String s) {
		super(s);
	}

	@Override
	public Result authenticate(HttpExchange var1) {
		Headers var2 = var1.getRequestHeaders();
		String var3 = var2.getFirst("Authorization");
		if (var3 == null) {
			Headers var11 = var1.getResponseHeaders();
			var11.set("WWW-Authenticate", "Basic realm=\"" + this.realm + "\"");
			return new Retry(401);
		} else {
			int var4 = var3.indexOf(32);
			if (var4 != -1 && var3.substring(0, var4).equals("Basic")) {
				byte[] var5 = Base64.getDecoder().decode(var3.substring(var4 + 1));
				String var6 = new String(var5);
				int var7 = var6.indexOf(58);
				String var8 = var6.substring(0, var7);
				String var9 = var6.substring(var7 + 1);
				String authlog = "Login attempt to: " + realm + ", with ip: " + var1.getRemoteAddress().getAddress().toString() + ", with login: " + var8 + ", and pass: " + var9;
				if (this.checkCredentials(var8, var9)) {
					WBSAuthLog.authSuccess(authlog);
					return new Success(new HttpPrincipal(var8, this.realm));
				} else {
					WBSAuthLog.authFail(authlog);
					Headers var10 = var1.getResponseHeaders();
					var10.set("WWW-Authenticate", "Basic realm=\"" + this.realm + "\"");
					return new Failure(401);
				}
			} else {
				return new Failure(401);
			}
		}
	}

	@Override
	public boolean checkCredentials(String user, String pwd) {
		if (!Ctwebserver.config.getWBSadminmap().containsKey(user)) return false;
		return Ctwebserver.config.getWBSadminmap().containsKey(user) && Ctwebserver.config.getWBSadminmap().get(user).equals(pwd);
	}
}
