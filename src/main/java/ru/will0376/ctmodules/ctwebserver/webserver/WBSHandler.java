package ru.will0376.ctmodules.ctwebserver.webserver;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.FMLCommonHandler;

import java.io.IOException;


public class WBSHandler implements HttpHandler {
	@Override
	public void handle(HttpExchange httpExchange) throws IOException {
		String encoding = "UTF-8";
		httpExchange.getResponseHeaders().set("Content-Type", "text/html; charset=" + encoding);
		StringBuilder builder = new StringBuilder();
		builder.append("<h1>Hello,").append(httpExchange.getPrincipal().getUsername()).append("</h1>");
		builder.append("<table border=\"1\">")
				.append("<tr>")
				.append("<td>").append("Players").append("</td>")
				.append("<td>").append("IP").append("</td>")
				.append("<td>").append("Position").append("</td>")
				.append("<td>").append("Screen").append("</td>")
				.append("<td>").append("Crash").append("</td>")
				.append("</tr>");

		for (EntityPlayerMP pl : FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayers()) {
			builder.append("<tr>")
					.append("<td>").append(pl.getName()).append("</td>")
					.append("<td>").append(pl.getPlayerIP().trim()).append("</td>")
					.append("<td>").append(pl.getPosition().toString().replace("BlockPos", "")).append("</td>")
					.append("<td>").append("<a href=\"/makeScreen?user=").append(pl.getName()).append("&admin=").append(httpExchange.getPrincipal().getUsername()).append("\">").append("<button>Screen</button>").append("</a>").append("</td>")
					.append("<td>").append("<a href=\"/makeCrash?user=").append(pl.getName()).append("&admin=").append(httpExchange.getPrincipal().getUsername()).append("\">").append("<button>Crash</button>").append("</a>").append("</td>")
					.append("</tr>");
		}
		builder.append("</table>");
		//if (Main.DEBUG) builder.append("<h1>DEBUG ON!</h1>");
		builder.append("</div>");
		WBSAbstraction.sendToHttpEx(httpExchange, builder);
	}
}

