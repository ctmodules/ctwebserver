package ru.will0376.ctmodules.ctwebserver.webserver;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.FMLCommonHandler;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

public class WBSAbstraction implements HttpHandler {
	public WBSAE wbsae;

	public static EntityPlayerMP getEPMP(String nick) {
		for (EntityPlayerMP player : FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayers())
			if (player.getName().toLowerCase().equals(nick.toLowerCase()))
				return player;
		return null;
	}

	public static void sendToHttpEx(HttpExchange httpExchange, StringBuilder builder) {
		try {
			byte[] bytes = builder.toString().getBytes();
			httpExchange.sendResponseHeaders(200, bytes.length);

			OutputStream os = httpExchange.getResponseBody();
			os.write(bytes);
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void handle(HttpExchange httpExchange) throws IOException {
	}

	public Map<String, String> queryToMap(String query) {
		Map<String, String> result = new HashMap<>();
		for (String param : query.split("&")) {
			String[] entry = param.split("=");
			if (entry.length > 1) {
				result.put(entry[0], entry[1]);
			} else {
				result.put(entry[0], "");
			}
		}
		return result;
	}

	public enum WBSAE {
		SCREEN("screen"),
		CRASH("crash");
		String classname;

		WBSAE(String classname) {
			this.classname = classname;
		}

		public String getClassname() {
			return classname;
		}
	}
}

