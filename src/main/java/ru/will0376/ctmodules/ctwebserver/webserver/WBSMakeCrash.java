package ru.will0376.ctmodules.ctwebserver.webserver;

import com.sun.net.httpserver.HttpExchange;
import net.minecraftforge.common.MinecraftForge;
import ru.will0376.ctmodules.events.EventRequestCrash;

import java.io.IOException;
import java.util.Map;

public class WBSMakeCrash extends WBSAbstraction {
	@Override
	public void handle(HttpExchange httpExchange) throws IOException {
		String encoding = "UTF-8";
		httpExchange.getResponseHeaders().set("Content-Type", "text/html; charset=" + encoding);
		Map<String, String> params = queryToMap(httpExchange.getRequestURI().getQuery());
		if (params.isEmpty()) return;
		MinecraftForge.EVENT_BUS.post(new EventRequestCrash("WBS", params.get("user")));
		StringBuilder builder = new StringBuilder();
		builder.append("<h1>Done. </h1>");
		builder.append("<a href=\"/").append("\">").append("<button>Back</button>").append("</a>");
		sendToHttpEx(httpExchange, builder);
	}
}

