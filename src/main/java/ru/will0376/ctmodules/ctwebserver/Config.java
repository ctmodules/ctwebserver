package ru.will0376.ctmodules.ctwebserver;

import net.minecraftforge.common.config.Configuration;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class Config {
	private final String WEBSERVER = "Webserver";
	private Configuration configuration;
	private List<String> authlist;
	private int timeout;
	private int port;
	private boolean enabled;

	public Config(File file) {
		this.configuration = new Configuration(file);
	}

	public void launch() {
		load();
		setConfigs();
		save();
	}

	private void load() {
		this.configuration.load();
	}

	private void save() {
		this.configuration.save();
	}

	private void setConfigs() {
		authlist = Arrays.asList(configuration.getStringList("authlist", WEBSERVER, new String[]{"nick:pass", "will0376:pass"}, "Syntax: \"Admin1:pass\"."));
		timeout = configuration.getInt("timeout", WEBSERVER, 10, 0, Integer.MAX_VALUE, "WBSTimeout(in sec)");
		port = configuration.getInt("port", WEBSERVER, 2016, 0, Integer.MAX_VALUE, "WebServer bind port");
		enabled = configuration.getBoolean("enabled", WEBSERVER, true, "use?");

	}

	public HashMap<String, String> getWBSadminmap() {
		HashMap<String, String> ret = new HashMap<>();
		if (authlist != null && !authlist.isEmpty()) {
			authlist.forEach(l -> ret.put(l.split(":")[0], l.split(":")[1]));
		}
		return ret;
	}

	public int getTimeout() {
		return timeout;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public int getPort() {
		return port;
	}
}
