package ru.will0376.ctmodules.ctwebserver;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import ru.will0376.ctmodules.events.utils.ChatForm;
import ru.will0376.ctmodules.events.utils.Utils;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Commands extends CommandBase {
	public static String usage =
			" /ctm-webserver reload\n" +
					" /ctm-webserver permissions";
	public static String aliases =
			" Aliases: [ctmwbs, ctm-wbs, ctmwebserver]";
	public static String permissions =
			" Permissions: [\n" +
					"   ctmodules.webserver.admin.reload\n" +
					" ]";

	@Override
	public String getName() {
		return "ctm-webserver";
	}

	@Override
	public String getUsage(ICommandSender sender) {
		return usage;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		if (args[0].equalsIgnoreCase("reload")) {
			if (!Utils.checkPermission(sender, "ctmodules.webserver.admin.reload")) return;
			if (Ctwebserver.config.isEnabled()) {
				Ctwebserver.webServer.stop(0);
				Ctwebserver.webServerTh = null;
				Ctwebserver.INSTANCE.runWebServer();
			} else sender.sendMessage(new TextComponentString(ChatForm.prefix_error + "WBS disabled."));
		} else if (args[0].equalsIgnoreCase("permissions"))
			sender.sendMessage(new TextComponentString(ChatForm.prefix + "Permissions: " + permissions));
		else sender.sendMessage(new TextComponentString(ChatForm.prefix + usage));
	}

	@Override
	public List getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos targetPos) {
		if (args.length == 1) {
			return getListOfStringsMatchingLastWord(args, "reload", "permissions");
		} else {
			return args.length >= 2 ? getListOfStringsMatchingLastWord(args, server.getOnlinePlayerNames()) : Collections.emptyList();
		}
	}

	@Override
	public List<String> getAliases() {
		ArrayList<String> al = new ArrayList();
		al.add("ctmwbs");
		al.add("ctm-wbs");
		al.add("ctmwebserver");
		return al;
	}
}
